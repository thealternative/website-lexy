export default {
  main: "Main",
  guides: "Anleitungen",
  bashGuide: "Bash",
  installGuide: "Install Guide",
  linuxAtETH: "Linux at ETH",
  courses: "Kursmaterialien",
  events: "Events",
  about: "Wer wir Sind",
  aboutUs: "Über Uns",
  contact: "Kontakt",
  name: "Name",
  mail: "Mail",
  message: "Nachricht",
  human: "Ich bin ein Mensch",
  robot: "Ich bin ein Roboter",
  submit: "Absenden",
  formIncomplete: "Formular Unvollständig",
  formIncompleteText: "Bitte fülle das gesamte Formular aus.",
  mailSuccess: "Formular Versandt",
  mailSuccessText: "Das Formular wurde erfolgreich versandt.",
  mailFailure: "Fehler",
  mailFailureText: "Etwas ist beim Versand des Formulars schiefgelaufen.",
  addEvent: "Event Hinzufügen",
  editEvent: "Event Bearbeiten",
  ENotEmpty: "Dieses Feld bitte ausfüllen",
  noAdblockTitle: "Kein Adblocker Installiert!",
  noAdblockText:
    "Du hast deinen Adblocker ausgeschaltet oder keinen installiert! Installiere jetzt einen für <a target='_blank' href='https://addons.mozilla.org/de/firefox/addon/ublock-origin/'>Firefox</a> oder <a target='_blank' href='https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm?hl=de'>Chrome</a>. Weil <a href='#philosophy'>Datenschutz</a> ein Menschenrecht ist.",
  stammtisch: "Stammtisch",
  date: "Datum",
  time: "Zeit",
  title: "Titel",
  speaker: "Sprecher*in",
  description: "Beschreibung",
  place: "Ort",
  signUpLink: "Anmdeldelink (falls extern)",
  signUp: "Anmelden",
  externalSignUp: "Anmelden (extern)",
  signedUpTitle: "Angemeldet",
  signedUpText: "Du bist für dieses Event angemeldet.",
  signOff: "Abmelden",
  viewParticipants: "Teilnehmendenliste",
  participants: "Teilnehmendenliste",
  copyMailAddresses: "Mailadressen Kopieren",
  loadTemplate: "Template Laden",
  sendReminder: "Reminder Versenden",
  sendMail: "Mail Versenden",
  copyMail: "Kopieren",
  noSignupNeeded: "Keine Anmeldung Nötig",
  scrollToTop: "Nach Oben",
  successfulSignup: "Anmeldung Erfolgreich",
  successfulSignupText: "Du bist jetzt für dieses Event angemeldet.",
  successfulSignoff: "Abmeldung Erfolgreich",
  successfulSignoffText: "Du bist nicht mehr für dieses Event angemeldet.",
  alreadySignedUp: "Bereits Angemeldet",
  alreadySignedUpText: "Du bist für dieses Event bereits angemeldet.",
  signInWithInstitution: "Über deine Institution anmelden",
  or: "Oder",
  yourEmail: "Deine Email",
  signInWithEmail: "Über Email anmelden",
  noEmail: "Email-Adresse fehlt",
  noEmailText: "Bitte gib noch eine Email-Adresse an",
  checkYourEmail: "Überprüfe deine Emails",
  checkYourEmailText: "Ein Anmeldelink wurde an deine Mailadresse versandt",
  copied: "Link Kopiert",
  copiedText: "Der Event-Link ist jetzt in deiner Zwischenablage.",
  signUps: "Anmeldungen",
};
