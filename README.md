This is the new website of TheAlternative, available at https://thealternative.ethz.ch.

[[_TOC_]]

## For Board Members

All content of the webpage is found in the `content/` folder. Specifically, the following files exist:

- `about.json`: This json file contains everyting in the "About Us" Section (except board members).
- `board.json`: This json file contains everything for the board listing.

<div class="panel panel-info">
**IMPORTANT**
{: .panel-heading}
<div class="panel-body">
All images referenced in `board.json` will have to be saved to `public/images`. Else, the website will display the theAlt logo instead.
</div>
</div>

- `eventTemplates.json`: When creating an Event, it is possible to select a template (e.g. descriptions, start/end time, place, etc.). These templates are stored in this file.
- `philosophy.json`: This file contains everyting in the definitions section at the bottom.
- `texts.json`: This file is the text in the main header.

## Local Setup

### Database

Make sure that a mariadb / mysql databsae is running and a database for the website exists.

Next, create a file `.env` with the following content and replace all content in `[]`:

```
SIP_AUTH_WEBSITE_CLIENT_ID=[VSETH Keycloak Client ID]
SIP_AUTH_WEBSITE_CLIENT_SECRET=[VSETH Keycloak Client Secret]
SIP_AUTH_OIDC_ISSUER=[VSETH Keycloak Issuer]

NEXTAUTH_URL=http://localhost:3000
NEXTAUTH_SECRET=sup3rs3cr3t

NEXT_PUBLIC_API_URL=http://localhost:3000
NEXT_PUBLIC_DEPLOYMENT=local

MAILER_HOST=[smpt.XXX.YYY]
MAILER_NAME=[Maxime Musterfrau]
MAILER_USERNAME=[maxmu@XXX.YYY]
MAILER_PASSWORD=[sup3rsecr3t]

DATABASE_URL="mysql://[username]:[password]@[host]:3306/[dbname]?schema=public"
```

Now, make sure that you're running Node.js 18 or later. One way to do this is via [nvm](https://github.com/nvm-sh/nvm):

```
nvm install 18
nvm use 18
```

Next, install all JavaScript dependencies with

```
npm install
```

Last, run the database migration with

```
npx prisma migrate dev
```

and run the local webserver

```
npm run dev
```

## Deployment

## Architecture

### Authentication / Authorization

### Translation

###
