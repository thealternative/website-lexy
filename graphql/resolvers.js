import prisma from "/lib/prisma";
import { getServerSession } from "next-auth/next";
import authOptions from "/lib/authOptions";

import hasAccess from "/utilities/hasAccess";
import {
  isInFuture,
  formatDateFromDB,
  formatTimeFromDB,
} from "/utilities/dates";
import { sendMail } from "/utilities/mail";

import config from "/thealternative.config";

export const resolvers = {
  Query: {
    getEvents: async () => {
      const events = await prisma.event.findMany();
      return events;
    },
    getFutureEvents: async (_, data, { session }) => {
      const admin = hasAccess(session, true);

      const events = await prisma.event.findMany({
        include: {
          signUps: {
            include: {
              user: {
                select: {
                  id: true,
                  name: admin,
                  email: admin,
                },
              },
            },
          },
        },
        orderBy: [{ date: "asc" }, { startTime: "asc" }],
      });
      const futureEvents = events.filter((event) => isInFuture(event));
      return futureEvents;
    },
    getUser: async (_, __, { session }) => {
      if (!session) return;

      return await prisma.user.findUnique({
        where: {
          email: session.email,
        },
      });
    },
  },
  Mutation: {
    addEvent: async (_, data, { session }) => {
      if (!hasAccess(session, true)) return false;

      const { time, ...dataWithoutTime } = data;
      const event = await prisma.event.create({
        data: {
          ...dataWithoutTime,
          startTime: data.time[0],
          endTime: data.time[1],
        },
      });
      return event ? true : false;
    },
    editEvent: async (_, data, { session }) => {
      if (!hasAccess(session, true)) return false;

      const { time, id, ...dataWithoutTime } = data;
      const event = await prisma.event.update({
        where: { id: data.id },
        data: {
          ...dataWithoutTime,
          startTime: data.time[0],
          endTime: data.time[1],
        },
      });
      return event ? true : false;
    },
    addSignUp: async (_, { id }, { session }) => {
      if (!hasAccess(session, false)) return false;

      const event = await prisma.event.findUnique({ where: { id } });
      const user = await prisma.user.findUnique({
        where: { email: session.email },
      });

      const signUps = await prisma.signUp.findMany({
        where: {
          eventId: id,
          userId: user.id,
        },
      });

      if (signUps.length > 0) return false;

      const signUp = await prisma.signUp.create({
        data: {
          eventId: id,
          userId: user.id,
        },
        include: {
          user: true,
        },
      });

      sendMail(
        user.email,
        config.address,
        config.signUpText
          .replace(
            "NAME",
            signUp.user.name
              ? signUp.user.name.split(" ")[0] || "Sir or Madam"
              : "Sir or Madam",
          )
          .replace("EVENT", event.title)
          .replace("DATE", formatDateFromDB(event.date, "en"))
          .replace("TIME", formatTimeFromDB(event.startTime, event.endTime))
          .replace("PLACE", event.place),
        config.signUpSubject,
      );

      return signUp ? true : false;
    },
    removeSignUp: async (_, { id }, { session }) => {
      if (!hasAccess(session, false)) return false;

      const event = await prisma.event.findUnique({ where: { id } });
      const user = await prisma.user.findUnique({
        where: { email: session.email },
      });

      const signUp = await prisma.signUp.findMany({
        where: {
          eventId: id,
          userId: user.id,
        },
      });

      if (signUp.length == 0) return false;

      await prisma.signUp.delete({
        where: {
          id: signUp[0].id,
        },
        include: {
          user: true,
        },
      });

      sendMail(
        user.email,
        config.address,
        config.signOffText
          .replace(
            "NAME",
            user.name
              ? user.name.split(" ")[0] || "Sir or Madam"
              : "Sir or Madam",
          )
          .replace("EVENT", event.title),
        config.signOffSubject,
      );

      return true;
    },
    sendReminder: async (_, { id }, { session }) => {
      if (!hasAccess(session, true)) return false;

      const event = await prisma.event.findUnique({
        where: { id },
        include: {
          signUps: {
            include: {
              user: true,
            },
          },
        },
      });

      const promises = event.signUps.map(async (signUp) => {
        await sendMail(
          signUp.user.email,
          config.address,
          config.reminderText
            .replace(
              "NAME",
              signUp.user.name
                ? signUp.user.name.split(" ")[0] || "Sir or Madam"
                : "Sir or Madam",
            )
            .replace("EVENT", event.title)
            .replace("DATE", formatDateFromDB(event.date, "en"))
            .replace("TIME", formatTimeFromDB(event.startTime, event.endTime))
            .replace("PLACE", event.place),
          config.reminderSubject,
        );
      });

      Promise.allSettled(promises).then(([result]) => {
        return true;
      });
      return true;
    },
  },
};
