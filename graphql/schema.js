import { DateTime } from "graphql-scalars";

export const typeDefs = `
  scalar DateTime

  type Event {
    id: Int
    title: String
    speaker: String
    description: String
    date: DateTime
    startTime: DateTime
    endTime: DateTime
    place: String
    signUp: String
    isStammtisch: Boolean
    signUps: [SignUp]
  }

  type SignUp {
    id: Int
    event: Event
    user: User
  }

  type User {
    id: String
    name: String
    email: String
    signUps: [SignUp]
  }

  type Query {
    getEvents: [Event]
    getFutureEvents: [Event]
    getUser: User
  }

  type Mutation {
    addEvent(title: String, speaker: String, description: String, date: DateTime, time: [DateTime], place: String, signUp: String, isStammtisch: Boolean): Boolean
    editEvent(id: Int, title: String, speaker: String, description: String, date: DateTime, time: [DateTime], place: String, signUp: String, isStammtisch: Boolean): Boolean
    addSignUp(id: Int): Boolean
    removeSignUp(id: Int): Boolean
    sendReminder(id: Int): Boolean
  }
`;
