import noah from "./images/board/placeholder.png";
import johanna from "./images/board/placeholder_girl.png";
import alex from "./images/board/alex.png";
import jeanclaude from "./images/board/jeanclaude.jpg";
import sophie from "./images/board/sophie.png";
import jindra from "./images/board/jindra.png";
import corinne from "./images/board/corinne.png";
import fadri from "./images/board/fadri.jpg";
import nico from "./images/board/nicolas.jpg";
import gianni from "./images/board/placeholder.png";

export default function imageLoader() {
  return {
    "Noah Marti": noah,
    "Johanna Polzin": johanna,
    "Alexander Schoch": alex,
    "Jean-Claude Graf": jeanclaude,
    "Sophie Eisenring": sophie,
    "Jindřich Dušek": jindra,
    "Corinne Furrer": corinne,
    "Fadri Lardon": fadri,
    "Nicolas König": nico,
    "Gianni Gugolz": gianni,
  };
}
