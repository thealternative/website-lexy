import Image from "next/image";
import getConfig from "next/config";
import { useRouter } from "next/navigation";

import { useI18n } from "/locales/client";

import {
  Avatar,
  Button,
  Card,
  Group,
  Text,
  useMantineTheme,
} from "@mantine/core";

import { IconMail } from "@tabler/icons-react";

import { getAccentColor } from "/utilities/colors";
import imageLoader from "/content/imageLoader";

import thealt_logo from "/public/images/thealt_signet.svg";
import thealt_logo_inv from "/public/images/thealt_signet_inv.svg";

export default function BoardMemberCard({ entry }) {
  const theme = useMantineTheme();
  const { locale, push } = useRouter();

  const t = useI18n();

  const sendMail = (address) => {
    push("mailto:" + address);
  };

  const copyMail = (address) => {
    navigator.clipboard.writeText(address);
  };

  const images = imageLoader();
  const image =
    images[entry.name] ||
    (theme.colorScheme === "dark" ? thealt_logo_inv : thealt_logo);

  return (
    <Card
      radius="md"
      withBorder
      p="lg"
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        flexGrow: 1,
      }}
      shadow="sm"
    >
      <div>
        <div
          style={{
            width: "120px",
            height: "120px",
            borderRadius: "1rem",
            margin: "auto",
            overflow: "hidden",
          }}
        >
          {/*image && (
            <Image
              src={image}
              width={0}
              height={0}
              sizes="100vw"
              style={{ width: "30rem", maxWidth: "100%", height: "auto" }}
              alt={entry.name}
            />
          )*/}
          {entry.image && (
            <img
              src={entry.image}
              style={{ width: "30rem", maxWidth: "100%", height: "auto" }}
              alt={entry.name}
            />
          )}
        </div>
        <Text ta="center" fz="lg" weight={500} mt="md">
          {entry.name}
        </Text>
        <Text ta="center" c="dimmed" fz="sm">
          <p style={{ margin: 0 }}>{entry.role[locale || "en"]}</p>
          <p
            onClick={() => copyMail(entry.mail)}
            style={{ cursor: "pointer", margin: 0 }}
          >
            {entry.mail ? entry.mail.replace("@", "[ät]") : ""}
          </p>
        </Text>
      </div>

      {entry.mail && (
        <Button
          leftSection={<IconMail color={getAccentColor(theme)} />}
          variant="default"
          mt="md"
          onClick={() => sendMail(entry.mail)}
          fullWidth
        >
          {t("sendMail")}
        </Button>
      )}
    </Card>
  );
}
