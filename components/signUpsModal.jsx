import { useState } from "react";

import { useI18n } from "/locales/client";

import { Button, Group, Modal, Table } from "@mantine/core";

import { gql, useMutation } from "@apollo/client";

const sendReminderMutation = gql`
  mutation sendReminder($id: Int) {
    sendReminder(id: $id)
  }
`;

export default function SignUpsModal({ open, close, event }) {
  const t = useI18n();
  const [sendReminder] = useMutation(sendReminderMutation);
  const [loading, setLoading] = useState(false);

  const copyMailAddresses = () => {
    const addresses = event.signUps
      .map((signUp) => signUp.user.email)
      .join(", ");
    navigator.clipboard.writeText(addresses);
  };

  const sendRem = async () => {
    setLoading(true);
    await sendReminder({
      variables: {
        id: event.id,
      },
    });
    setLoading(false);
  };

  if (!event) return <></>;

  return (
    <Modal opened={open} onClose={close} size="xl" title={t("participants")}>
      <Table striped highlightOnHover>
        <Table.Thead>
          <Table.Tr>
            <Table.Th>Name</Table.Th>
            <Table.Th>Email</Table.Th>
          </Table.Tr>
        </Table.Thead>
        <Table.Tbody>
          {event.signUps.map((signUp, i) => (
            <Table.Tr key={i}>
              <Table.Td>{signUp.user.name}</Table.Td>
              <Table.Td>{signUp.user.email}</Table.Td>
            </Table.Tr>
          ))}
        </Table.Tbody>
      </Table>

      <Group grow mt="md">
        <Button onClick={copyMailAddresses}>{t("copyMailAddresses")}</Button>
        <Button onClick={sendRem} loading={loading}>
          {t("sendReminder")}
        </Button>
      </Group>
    </Modal>
  );
}
