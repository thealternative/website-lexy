import { useRouter } from "next/router";
import Image from "next/image";
import dynamic from "next/dynamic";

import {
  Alert,
  Grid,
  MediaQuery,
  Space,
  useMantineTheme,
  useComputedColorScheme,
} from "@mantine/core";

import {
  IconChevronDown,
  IconDeviceLaptop,
  IconMapPin,
} from "@tabler/icons-react";

import { useCurrentLocale } from "/locales/client";

import parse from "html-react-parser";

const NoAdblockBanner = dynamic(() => import("../components/noAdblockBanner"), {
  ssr: false,
});
import { getAccentColor } from "/utilities/colors";

import content from "/content/texts.json";
import tux from "/public/images/tux.png";
import logo from "/public/images/logo.svg";
import logo_inv from "/public/images/logo-inv.svg";

export default function Header() {
  const theme = useMantineTheme();
  const cs = useComputedColorScheme();
  const locale = useCurrentLocale();

  return (
    <div
      className="flex flex-col justify-evenly"
      style={{
        height: "calc(100vh - 112px)",
      }}
    >
      <NoAdblockBanner />
      <div className="flex gap-4 items-center">
        <div>
          <div>
            <Image
              src={cs === "dark" ? logo_inv : logo}
              width={0}
              height={0}
              sizes="100vw"
              style={{ width: "30rem", maxWidth: "100%", height: "auto" }}
              alt="TheAlternative Logo  "
              className="mb-8"
            />
            {content.topSection.map((entry, i) => (
              <p key={i}>{parse(entry[locale || "en"])}</p>
            ))}
            <Alert
              title={content.officeHours.title[locale || "en"]}
              icon={<IconDeviceLaptop />}
              className="mt-8"
            >
              <span>{content.officeHours.text[locale || "en"]}</span>
              <Alert
                title={content.officeHours.datetimeplace[locale || "en"]}
                icon={<IconMapPin />}
                variant="transparent"
              />
            </Alert>
          </div>
        </div>
        <div className="hidden md:block">
          <img
            src="/images/tux.png"
            style={{
              width: "100%",
              height: "auto",
              filter: "drop-shadow(0px 0px 250px rgba(255, 255, 255, 0.4))",
            }}
            alt="Tux, the Linux Mascot"
          />
        </div>
      </div>
      <div className="flex justify-center">
        <a href="#events">
          <IconChevronDown
            color={cs === "light" ? getAccentColor(theme) : "white"}
            size={72}
            className="animate-bounce"
          />
        </a>
      </div>
    </div>
  );
}
