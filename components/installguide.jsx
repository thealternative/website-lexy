export default function InstallGuide() {
  return (
    <>
      <h1 data-number={1} id="introduction">
        <span className="header-section-number">1</span> Introduction
      </h1>
      <p>
        This is a concise guide to install Linux. It can be used as a standalone
        guide, although we recommend that you attend our Install Events during
        the LinuxDays so that experienced users can help you with your
        installation.
      </p>
      <p>
        The guide help you to create an install stick, prepare your existing
        operating system and finally install Linux. We also included some links
        to distribution-specific installation resources, for the most up-to-date
        instructions.
      </p>
      <p>At the end of the guide, we include some software recommendations.</p>
      <h2 data-number="1.1" id="overview">
        <span className="header-section-number">1.1</span> Overview
      </h2>
      <p>Installing Linux is done in three steps:</p>
      <ul>
        <li>Flash an USB Stick with the Linux installer</li>
        <li>Prepare your existing operating system</li>
        <li>Install Linux</li>
      </ul>
      <p>
        We will help you at each step. If you encounter problems or have
        suggestions how to improve this guide feel free to reach out!
      </p>
      <h2 data-number="1.2" id="linuxdays-install-event">
        <span className="header-section-number">1.2</span> LinuxDays Install
        Event
      </h2>
      <p>
        <em>
          You can skip this part if you do not physically attend the install
          events
        </em>
      </p>
      <p>
        Welcome to the LinuxDays Install Event! We are happy you could make it.
      </p>
      <h3 data-number="1.2.1" id="staff">
        <span className="header-section-number">1.2.1</span> Staff
      </h3>
      <p>
        Our staff is easily recognizable by their blue T-Shirts. Feel free to
        ask them about Linux, TheAlternative, Free Software or anything else.
      </p>
      <p>
        We will help you if you are unable to proceed with the install guide or
        have any other questions. If we do not have an immediate answer, we know
        who to ask or where to look it up.
      </p>
      <p>Also, feel free to stay and just chat :)</p>
      <h3 data-number="1.2.2" id="demolaptops">
        <span className="header-section-number">1.2.2</span> Demolaptops
      </h3>
      <p>
        We have prepared some laptops with different distribution and desktop
        environments. You can try them out, and decide what you like best.
      </p>
      <p>
        If you do not really care or can not decide, we will try to recommend
        something based on what you want to use Linux for.
      </p>
      <h3 data-number="1.2.3" id="usb-install-sticks-install-guide-copies">
        <span className="header-section-number">1.2.3</span> USB Install Sticks
        &amp; Install Guide Copies
      </h3>
      <p>
        We have prepared USB Install Sticks so you can get started right away
        with installing Linux.
      </p>
      <p>
        Further, we have printed copies of the Install Guide if you rather have
        a printed version than look at it on you phone.
      </p>
      <h3 data-number="1.2.4" id="anything-else">
        <span className="header-section-number">1.2.4</span> Anything else?
      </h3>
      <p>We have adapters (like USB to LAN), hubs, mouse and keyboards, …</p>
      <h1 data-number={2} id="flash-stick">
        <span className="header-section-number">2</span> Flash Stick
      </h1>
      <p>You will prepare a stick from which you can install Linux from.</p>
      <p>
        <em>
          If you are at one of our Install Events, we already prepared USB
          sticks. Choose a distribution (read on) then come to the welcome desk
          to get your stick.
        </em>
      </p>
      <h2 data-number="2.1" id="choosing-a-distribution">
        <span className="header-section-number">2.1</span> Choosing a
        distribution
      </h2>
      <p>In the words of Richard Stallman:</p>
      <blockquote>
        <p>
          What you’re referring to as Linux, is in fact, GNU/Linux, or as I’ve
          recently taken to calling it, GNU plus Linux. Linux is not an
          operating system unto itself, but rather another free component of a
          fully functioning GNU system &gt; made useful by the GNU corelibs,
          shell utilities and vital system components comprising a full OS as
          defined by POSIX."
        </p>
      </blockquote>
      <p>
        In simpler terms: Linux by itself is not a full operating system (rather
        than a vital technical part of it). You need to choose a distribution (a
        “wrapper” around Linux) which will define to a large extend how you as a
        user will interact with the operating system (like installing new
        programs, how the desktop looks by default, …).
      </p>
      <p>
        As choosing a distribution is heavily opiniated, we describe it here
        very short. Feel free to reach out to us for a personal recommendation
        based on your specific needs.
      </p>
      <p>
        If you “just want Linux”, we recommend you go with{" "}
        <a href="https://ubuntu.com/">Ubuntu</a>. As a more open but also very
        stable alternative we recommend{" "}
        <a href="https://getfedora.org/">Fedora</a>. Truly Free (as in Freedom!)
        is <a href="https://www.freebsd.org/">FreeBSD</a>.
      </p>
      <p>
        For each of these distributions we give you starting points for a
        successful installation. If you want to install other distributions feel
        free to do so; and we will do our best to help you. If you are an
        enthusiast and want to try to install something more advanced (like{" "}
        <a href="https://www.archlinux.org/">Arch Linux</a>) come join us :).
      </p>
      <h2 data-number="2.2" id="download-iso">
        <span className="header-section-number">2.2</span> Download ISO
      </h2>
      <p>
        You need to download an image containing the operating system installer.
        Visit the webpage of your favourite distribution and download it.
      </p>
      <p>
        If you have to choose from multiple versions, here some guidance of
        commonly used terms:
      </p>
      <ul>
        <li>
          <code>amd64</code>, <code>i386</code>, … refers to the architecture of
          your device. If you have a laptop / PC, it is very likely that you
          need <code>amd64</code> (else you’d probably know it).
        </li>
        <li>
          <code>LTS</code> stands for long term support; meaning this specific
          version of the distribution will receive security updates for much
          longer than other versions.
        </li>
      </ul>
      <h2 data-number="2.3" id="create-install-usb-stick">
        <span className="header-section-number">2.3</span> Create install USB
        Stick
      </h2>
      <p>
        Get a USB stick of at least 8 Gb (make sure you do not need the data on
        it anymore!). Flash (=“Put”) the .iso file you have just downloaded on
        the USB stick.
      </p>
      <p>
        On windows, you can use <a href="https://rufus.ie/">rufus</a>. On macOS,
        you can use <a href="https://www.balena.io/etcher/">etcher</a>.
      </p>
      <h1 data-number={3} id="prepare-windows-mac">
        <span className="header-section-number">3</span> Prepare Windows / Mac
      </h1>
      <p>
        If you want to keep your existing operating system (Windows or macOS)
        you need to prepare it. This will allow you to “double-boot”, hence
        choose at startup which operating system to run.
      </p>
      <p>
        If you do not want to keep your existing operating system, you can skip
        this section.
      </p>
      <h2 data-number="3.1" id="about-partitioning">
        <span className="header-section-number">3.1</span> About Partitioning
      </h2>
      <p>
        Disk partitioning is the separation of different regions on a hard drive
        or an SSD. This separation is used to install two different operating
        systems on a computer or simply to have different regions for different
        purposes (like a partition for your data and one for the programs).
      </p>
      <p>
        We recommend to have at least 40GB of space partitioned for Linux. If
        you know you need programs for your studies (e.g.&nbsp;Matlab, which is
        around 30GB), please have enough free disk space to install the desired
        programs and probably some extra, just in case.
      </p>
      <p>
        Be aware that repartitioning later is practically impossible, so try to
        be on the safe side. If you do not have space constraints, with 100Gb
        all you wildest wishes should be fulfillable.
      </p>
      <h2 data-number="3.2" id="windows-10">
        <span className="header-section-number">3.2</span> Windows 10
      </h2>
      <p>If you have Windows (and want to keep it) follow on.</p>
      <h3 data-number="3.2.1" id="making-space-for-linux">
        <span className="header-section-number">3.2.1</span> Making Space for
        Linux
      </h3>
      <p>Shrink Windows partition:</p>
      <ul>
        <li>
          Open <code>create and format hard disk partitions</code> or{" "}
          <code>diskmgmt.msc</code>.
        </li>
        <li>Look for the large Windows partition at the bottom.</li>
        <li>
          Right-click on it and select <code>Shrink Volume...</code>
        </li>
        <li>Shrink it so you have enough space available.</li>
      </ul>
      <p>
        If you are unable to shrink the partition, use a tool like{" "}
        <code>EaseUS Partition Master</code> or try it in the Linux installer.
        Ask a helper for assistance.
      </p>
      <p>
        If you do not have enough space, you can free up disk space with these
        tips:
      </p>
      <ul>
        <li>
          Open <code>Disk Cleanup</code> and click on{" "}
          <code>Clean up system files</code>. Select all that apply.
        </li>
        <li>
          Open <code>Add or Remove Programs</code> and remove unneeded programs
        </li>
        <li>
          Download <code>WinDirStat</code> from https://windirstat.net/ and find
          where the large files are hiding
        </li>
      </ul>
      <h3 data-number="3.2.2" id="disable-bitlocker">
        <span className="header-section-number">3.2.2</span> Disable Bitlocker
      </h3>
      <p>
        Bitlocker encrypts your disk, but unfortunately it is not compatible
        with Linux boot loaders.
      </p>
      <p>Disable Bitlocker:</p>
      <ul>
        <li>
          Open <code>cmd</code> as administrator: Press Windows Key, type{" "}
          <code>cmd</code>, then right click on <code>Command Prompt</code> and
          select <code>Run as administrator</code>)
        </li>
        <li>
          Type <code>manage-bde -status</code> and look for{" "}
          <code>Conversion Status: Fully Decrypted</code>
        </li>
        <li>
          For any drive which is not <code>Fully Decrypted</code>, type{" "}
          <code>manage-bde -unlock C:</code> (replace <code>C:</code> with the
          drive you need to decrypt), and press Enter.
        </li>
        <li>
          Wait for decryption to end (execute <code>manage-bde -status</code>{" "}
          for status updates)
        </li>
      </ul>
      <p>
        You can also try to disable Bitlocker over the UI (look for{" "}
        <code>Manage Bitlocker</code>) but it has been confusing to do so in the
        last few years.
      </p>
      <h3 data-number="3.2.3" id="fast-boot">
        <span className="header-section-number">3.2.3</span> Fast Boot
      </h3>
      <p>
        <code>Fast boot</code> allows Windows to boot faster, but when it is
        active, the windows file system will be inaccessible to other OSes (like
        Linux).
      </p>
      <p>
        You can avoid the <code>Fast boot</code> mode by explicitly choosing to{" "}
        <code>Restart</code> (rather than <code>Shut down</code>) in windows.
      </p>
      <p>Alternatively, you can simply disable fast boot:</p>
      <ul>
        <li>Find the battery icon in the task bar</li>
        <li>
          Right-click and select <code>Power Options</code>
        </li>
        <li>
          Select <code>Choose what the power buttons do</code>
        </li>
        <li>
          Select <code>Change settings that are currently unavailable</code>
        </li>
        <li>
          Remove the check mark at <code>Turn on fast startup</code>
        </li>
      </ul>
      <h3 data-number="3.2.4" id="booting-the-installer">
        <span className="header-section-number">3.2.4</span> Booting the
        installer
      </h3>
      <p>Make sure your Linux install stick is plugged into your PC.</p>
      <p>
        Click <code>Restart</code> in the Windows start menu, while holding down
        the <code>Shift</code> key. A <code>Please wait</code>-text appears,
        after which Windows should enter the troubleshooting menu.
      </p>
      <p>
        From this menu you can choose a startup device, or enter the UEFI
        settings.
      </p>
      <ul>
        <li>
          To choose the startup device, select <code>Use a device</code>. As we
          want to boot from the Linux USB stick choose <code>USB FDD</code> or
          similar.
        </li>
        <li>
          To enter the UEFI settings, select <code>Troubleshoot</code>,{" "}
          <code>Advanced Options</code> and then{" "}
          <code>UEFI Firmware Settings</code>.
        </li>
      </ul>
      <p>
        If none of this works, you can try to change the startup device
        immediately after booting. This guide will explain how in the next
        chapter.
      </p>
      <h2 data-number="3.3" id="macos">
        <span className="header-section-number">3.3</span> macOS
      </h2>
      <p>
        <span style={{ color: "red" }}>
          {" "}
          <em>
            Note that on modern Apple computers it is generally not advisable to
            install Linux (MacBooks after 2016 simply are not very compatible
            with Linux.)
          </em>{" "}
        </span>
      </p>
      <p>
        Up-to-date information on the status of Linux support on MacBooks can be
        found <a href="https://github.com/Dunedan/mbp-2016-linux">here</a>.
      </p>
      <h3 data-number="3.3.1" id="shrink-your-os-x-partition">
        <span className="header-section-number">3.3.1</span> Shrink your OS X
        partition
      </h3>
      <p>
        On OS X, we will resize your existing partition to make space for your
        new Linux system.
      </p>
      <ul>
        <li>
          Go to <code>/Applications/Utilities</code> and open the{" "}
          <code>Disk Utility</code>.
        </li>
        <li>
          Select your main disk in the list to the left (usually the first or
          the largest).
        </li>
        <li>
          Select the tab <code>Partition</code>.
        </li>
        <li>
          On the left side there is a white rectangle with some blue threshold
          indicating the space consumed by Mac OS X.
        </li>
        <li>
          Click and hold the lower right corner of that rectangle and draw the
          cursor upwards, to shrink the volume.
        </li>
        <li>
          The text on the right will give you information about the freed
          memory.
        </li>
        <li>
          Once you are satisfied, click <code>Apply</code> to confirm the
          shrinking operation.
        </li>
      </ul>
      <h3 data-number="3.3.2" id="install-refind">
        <span className="header-section-number">3.3.2</span> Install rEFInd
      </h3>
      <p>
        We will install the bootloader on OS X before the Linux installation.{" "}
        <em>rEFInd</em> will boot your USB Install Medium as well as your
        operating system later on.
      </p>
      <ul>
        <li>
          Browse the web for{" "}
          <a href="http://www.rodsbooks.com/refind/getting.html">
            http://www.rodsbooks.com/refind/getting.html
          </a>{" "}
          and scroll down a bit to click the link <em>A binary zip file</em>.
        </li>
        <li>
          Once the download has completed, go to the <em>Download folder</em>{" "}
          and extract the file.
        </li>
        <li>
          Open a Mac terminal by going to <code>Application/Utilities</code> and
          opening <code>Terminal</code>.
        </li>
        <li>
          Type <code>cd</code> (with a space afterwards, but do{" "}
          <strong>not</strong> press <em>Enter</em> yet) and drag the extracted
          folder into the terminal.
        </li>
        <li>
          Now, hit <em>Enter</em> and then type <code>./refind-install</code>.
        </li>
        <li>
          When prompted for your password, enter it. (You won’t see what you
          enter at the password prompt at all. You have to enter your password
          blindly.)
        </li>
      </ul>
      <h3 data-number="3.3.3" id="troubleshoot">
        <span className="header-section-number">3.3.3</span> Troubleshoot
      </h3>
      <p>
        In case you get an error message saying <em>ALERT: SIP ENABLED</em>, you
        need to do the following:
      </p>
      <ul>
        <li>
          Turn off your Mac and start it again, pressing <code>Cmd + R</code>{" "}
          immediately after turning it on again (you might want to hold it for a
          while).
        </li>
        <li>
          Your Mac should boot in <code>recovery mode</code>. Go to{" "}
          <code>Utilities</code> and open up a terminal.
        </li>
        <li>
          Now type: <code>csrutil disable</code>.
        </li>
        <li>
          Then reboot back into your normal system and try to install{" "}
          <em>rEFInd</em> again.
        </li>
        <li>
          After installation, feel free to go once more into recovery and type{" "}
          <code>csrutil enable</code>.
        </li>
      </ul>
      <h3 data-number="3.3.4" id="booting-the-installer-1">
        <span className="header-section-number">3.3.4</span> Booting the
        installer
      </h3>
      <p>We will now boot from your USB Install Medium.</p>
      <ul>
        <li>Plug your USB Install Medium into your laptop.</li>
        <li>
          Reboot your machine and <em>rEFInd</em> should see the USB key.
        </li>
        <li>
          Your USB installer may have several ways to boot. They all will show
          up and have a little USB icon to the bottom right.
        </li>
        <li>
          Usually the first USB installer is correct. If it doesn’t work, reboot
          and try the others.
        </li>
        <li>
          If your USB installer doesn’t show up, hit <code>Escape</code> to
          refresh the list. It should show up now.
        </li>
        <li>
          If you see multiple USB keys, try each of them until you find the
          Linux installer.
        </li>
      </ul>
      <h1 data-number={4} id="install">
        <span className="header-section-number">4</span> Install
      </h1>
      <p>You are all set to install Linux on your device.</p>
      <p>
        If you have not yet done so; ensure you have backed up all your data! In
        the (unlikely) case something goes terribly wrong, you can recover your
        data.
      </p>
      <h2 data-number="4.1" id="boot-from-stick">
        <span className="header-section-number">4.1</span> Boot from Stick
      </h2>
      <p>
        You now want to boot from the USB stick (if you have not already done
        so, depending on your existing operating system).
      </p>
      <p>
        This step can be tricky, as the way to accomplish this is different over
        different devices and brands. Generally you need to look for something
        like “BIOS” or “startup device”. You then need to choose the USB stick
        as the startup device.
      </p>
      <p>
        You can enter the BIOS or change the startup device usually with the
        function keys (such as F1, F8, F10, F12) or with other keys like ESC or
        Enter.
      </p>
      <p>The most common keys by brand: - Lenovo: Enter - HP: Esc</p>
      <h2 data-number="4.2" id="partitioning">
        <span className="header-section-number">4.2</span> Partitioning
      </h2>
      <p>
        This can either be done automatically by the Linux distributions
        installer, or manually if you want more control over your setup. If you
        are unsure about what setup you want, it might be best to follow the
        automatic setup.
      </p>
      <figure>
        <img
          src="images/partitions.png"
          alt="Example partitions for a system with Linux only"
        />
        <figcaption aria-hidden="true">
          Example partitions for a system with Linux only
        </figcaption>
      </figure>
      <figure>
        <img
          src="images/partitions2.jpg"
          alt="Example partitions for a dual boot system"
        />
        <figcaption aria-hidden="true">
          Example partitions for a dual boot system
        </figcaption>
      </figure>
      <h3 data-number="4.2.1" id="automatic-setup">
        <span className="header-section-number">4.2.1</span> Automatic setup
      </h3>
      <p>
        The various installers usually offer an option to either install
        alongside an existing operating system (Windows/Mac), or to use the
        entire disk and erase previously installed systems. If you choose to
        install alongside something else, there is usually an option to choose
        how much space you want to allocate for the new system. Note that the
        partition for the old system will also have to shrink by this amount.
      </p>
      <h3 data-number="4.2.2" id="manual-setup">
        <span className="header-section-number">4.2.2</span> Manual setup
      </h3>
      <p>
        If you want a manual setup these are some partitions you might consider:
      </p>
      <ul>
        <li>
          <code>/boot</code>: This is the boot partition for EFI. This already
          exists and will be reused when installing your new operating system.
        </li>
        <li>
          <code>/</code> (root). A root partition is required for all
          installations. This is where all the operating system files live.
        </li>
        <li>
          <code>/home</code>: Some users like to their home directory on a
          different partition. This can make re-installation of a distribution
          easier. However, this is entirely optional.
        </li>
        <li>
          <code>/swap</code>: A swap partition is used as an extension for your
          machines memory, if it ever fills up. Nowadays, a lot of people omit
          this on personal computers.
        </li>
      </ul>
      <p>
        You will also be able to choose a file system for <code>/</code> and{" "}
        <code>/home</code>. Almost always the <code>ext4</code> filesystem
        should be used. If you know what you are doing, you can of course also
        choose something else.
      </p>
      <h2 data-number="4.3" id="ubuntu">
        <span className="header-section-number">4.3</span> Ubuntu
      </h2>
      <p>
        We recommend to follow the{" "}
        <a href="https://ubuntu.com/tutorials/tutorial-install-ubuntu-desktop">
          official install-guide
        </a>{" "}
        for installing Ubuntu.
      </p>
      <p>
        As further resources, we recommend the{" "}
        <a href="https://wiki.ubuntu.com/">Ubuntu Wiki</a> and the{" "}
        <a href="https://askubuntu.com/">askubuntu StackExchange</a>.
      </p>
      <h2 data-number="4.4" id="fedora">
        <span className="header-section-number">4.4</span> Fedora
      </h2>
      <p>
        The top-level documentation for Fedora can be found on the official{" "}
        <a href="https://docs.fedoraproject.org/en-US/docs/">
          Fedora docs website
        </a>
        .
      </p>
      <p>The following links are a good starting point:</p>
      <ul>
        <li>
          The Fedora{" "}
          <a href="https://docs.fedoraproject.org/en-US/fedora/f31/install-guide/Introduction/">
            install guide
          </a>
        </li>
        <li>
          The{" "}
          <a href="https://docs.fedoraproject.org/en-US/quick-docs/">
            usage and customization guide
          </a>
        </li>
        <li>
          The guide for upgrading to a new{" "}
          <a href="https://docs.fedoraproject.org/en-US/quick-docs/dnf-system-upgrade/">
            release
          </a>
        </li>
      </ul>
      <h2 data-number="4.5" id="open-suse">
        <span className="header-section-number">4.5</span> Open SUSE
      </h2>
      <p>
        For getting started with OpenSuse we recommend going through{" "}
        <a href="https://www.opensuse-guide.org">Unofficial openSUSE Guide</a>,
        since it gives a good overview and can save you some time.
      </p>
      <p>
        Also check out the{" "}
        <a href="https://doc.opensuse.org/documentation/leap/startup/single-html/book.opensuse.startup/index.html">
          Start-Up Guide
        </a>{" "}
        from their website and the{" "}
        <a href="https://en.opensuse.org/Portal:Documentation">documentation</a>{" "}
        if you have further questions or want to work with the official
        material.
      </p>
      <h1 data-number={5} id="software">
        <span className="header-section-number">5</span> Software
      </h1>
      <p>
        These are software recommendations for Linux. Most of these are
        graphical programs, but there are some programs that run in the terminal
        only. Note that these programs have various licenses, some might not
        even be “libre” software (due to them becoming closed source at a later
        point, or this list having errors)!
      </p>
      <p>
        Some programs are annotated if they are best used on GNOME or KDE. This
        does not mean they cannot be used on the other environment, but the
        visual integration might be lacking in these cases.
      </p>
      <h2 data-number="5.1" id="office">
        <span className="header-section-number">5.1</span> Office
      </h2>
      <table>
        <colgroup>
          <col style={{ width: "33%" }} />
          <col style={{ width: "33%" }} />
          <col style={{ width: "33%" }} />
        </colgroup>
        <thead>
          <tr className="header">
            <th style={{ textAlign: "left" }}>Use case</th>
            <th style={{ textAlign: "left" }}>Application</th>
            <th style={{ textAlign: "left" }}>Comment</th>
          </tr>
        </thead>
        <tbody>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>Office suite</td>
            <td style={{ textAlign: "left" }}>LibreOffice</td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="even">
            <td style={{ textAlign: "left" }}>Office suite</td>
            <td style={{ textAlign: "left" }}>OnlyOffice</td>
            <td style={{ textAlign: "left" }}>
              Looks much nicer than LibreOffice
            </td>
          </tr>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>PDF viewer</td>
            <td style={{ textAlign: "left" }}>Zathura</td>
            <td style={{ textAlign: "left" }}>
              All desktop environments include a PDF viewer. Zathura is another,
              more simple option.
            </td>
          </tr>
          <tr className="even">
            <td style={{ textAlign: "left" }}>Note taking, PDF annotation</td>
            <td style={{ textAlign: "left" }}>Xournal</td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>LaTeX editor</td>
            <td style={{ textAlign: "left" }}>
              Texstudio, Texmaker, GNOME LaTeX
            </td>
            <td style={{ textAlign: "left" }}>
              Texstudio/Texmaker are “what you see is what you get”, GNOME LaTeX
              uses a “write, then compile” workflow.
            </td>
          </tr>
          <tr className="even">
            <td style={{ textAlign: "left" }}>Mindmapping</td>
            <td style={{ textAlign: "left" }}>vym</td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>Design/Publishing</td>
            <td style={{ textAlign: "left" }}>Scribus</td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="even">
            <td style={{ textAlign: "left" }}>Simple Text Editor</td>
            <td style={{ textAlign: "left" }}>
              Gedit/Geany (GNOME), Kwrite/Kate (KDE)
            </td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>Advanced Text Editor</td>
            <td style={{ textAlign: "left" }}>vim, emacs</td>
            <td style={{ textAlign: "left" }}>
              These are a bit more difficult to learn, but are very popular with
              power users.
            </td>
          </tr>
          <tr className="even">
            <td style={{ textAlign: "left" }}>File manager</td>
            <td style={{ textAlign: "left" }}>
              PCManFM, ranger/nnn (Terminal)
            </td>
            <td style={{ textAlign: "left" }}>
              All desktop environments include a file manager, these are other
              more advanced options.
            </td>
          </tr>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>CAD</td>
            <td style={{ textAlign: "left" }}>FreeCAD</td>
            <td style={{ textAlign: "left" }} />
          </tr>
        </tbody>
      </table>
      <h2 data-number="5.2" id="graphics">
        <span className="header-section-number">5.2</span> Graphics
      </h2>
      <table>
        <colgroup>
          <col style={{ width: "33%" }} />
          <col style={{ width: "33%" }} />
          <col style={{ width: "33%" }} />
        </colgroup>
        <thead>
          <tr className="header">
            <th style={{ textAlign: "left" }}>Use case</th>
            <th style={{ textAlign: "left" }}>Application</th>
            <th style={{ textAlign: "left" }}>Comment</th>
          </tr>
        </thead>
        <tbody>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>Image editing</td>
            <td style={{ textAlign: "left" }}>GIMP, Pinta</td>
            <td style={{ textAlign: "left" }}>
              GIMP is a more fully featured editor, similar to Photoshop. Pinta
              is inspired by Paint.NET.
            </td>
          </tr>
          <tr className="even">
            <td style={{ textAlign: "left" }}>Painting</td>
            <td style={{ textAlign: "left" }}>Krita</td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>3D Animation</td>
            <td style={{ textAlign: "left" }}>Blender</td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="even">
            <td style={{ textAlign: "left" }}>Photo management</td>
            <td style={{ textAlign: "left" }}>Darktable, Digikam</td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>Scanning</td>
            <td style={{ textAlign: "left" }}>
              Skanlite (KDE), SimpleScan (GNOME)
            </td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="even">
            <td style={{ textAlign: "left" }}>OCR (Text Recognition)</td>
            <td style={{ textAlign: "left" }}>OCRGui, OCRmyPDF (Terminal)</td>
            <td style={{ textAlign: "left" }} />
          </tr>
        </tbody>
      </table>
      <h2 data-number="5.3" id="multimedia">
        <span className="header-section-number">5.3</span> Multimedia
      </h2>
      <table>
        <colgroup>
          <col style={{ width: "33%" }} />
          <col style={{ width: "33%" }} />
          <col style={{ width: "33%" }} />
        </colgroup>
        <thead>
          <tr className="header">
            <th style={{ textAlign: "left" }}>Use case</th>
            <th style={{ textAlign: "left" }}>Application</th>
            <th style={{ textAlign: "left" }}>Comment</th>
          </tr>
        </thead>
        <tbody>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>Video Player</td>
            <td style={{ textAlign: "left" }}>mpv, VLC, Totem (GNOME)</td>
            <td style={{ textAlign: "left" }}>
              Desktop environments have a default video player, but mpv/VLC are
              usually more powerful.
            </td>
          </tr>
          <tr className="even">
            <td style={{ textAlign: "left" }}>Audio editing</td>
            <td style={{ textAlign: "left" }}>Audacity</td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>MP3 tagging</td>
            <td style={{ textAlign: "left" }}>Picard</td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="even">
            <td style={{ textAlign: "left" }}>
              Digital Audio Workstation (DAW)
            </td>
            <td style={{ textAlign: "left" }}>Ardour</td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>YouTube downloader</td>
            <td style={{ textAlign: "left" }}>youtube-dl (Terminal)</td>
            <td style={{ textAlign: "left" }}>
              Requires regular updates, because YouTube changes their website
              rather often
            </td>
          </tr>
          <tr className="even">
            <td style={{ textAlign: "left" }}>Screen recording</td>
            <td style={{ textAlign: "left" }}>OBS, SimpleScreenRecorder</td>
            <td style={{ textAlign: "left" }}>
              OBS is very powerful, SimpleScreenRecorder is more simple option.
            </td>
          </tr>
        </tbody>
      </table>
      <h2 data-number="5.4" id="scientific">
        <span className="header-section-number">5.4</span> Scientific
      </h2>
      <table>
        <colgroup>
          <col style={{ width: "33%" }} />
          <col style={{ width: "33%" }} />
          <col style={{ width: "33%" }} />
        </colgroup>
        <thead>
          <tr className="header">
            <th style={{ textAlign: "left" }}>Use case</th>
            <th style={{ textAlign: "left" }}>Application</th>
            <th style={{ textAlign: "left" }}>Comment</th>
          </tr>
        </thead>
        <tbody>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>Matlab</td>
            <td style={{ textAlign: "left" }}>Python+Numpy, Octave</td>
            <td style={{ textAlign: "left" }}>
              Octave tries to be similar to Matlab, while Python+Numpy is rather
              different.
            </td>
          </tr>
          <tr className="even">
            <td style={{ textAlign: "left" }}>Statistics</td>
            <td style={{ textAlign: "left" }}>R, PSPP</td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>ArcGIS</td>
            <td style={{ textAlign: "left" }}>QGIS, GRASS GIS</td>
            <td style={{ textAlign: "left" }} />
          </tr>
        </tbody>
      </table>
      <h2 data-number="5.5" id="internet">
        <span className="header-section-number">5.5</span> Internet
      </h2>
      <table>
        <colgroup>
          <col style={{ width: "33%" }} />
          <col style={{ width: "33%" }} />
          <col style={{ width: "33%" }} />
        </colgroup>
        <thead>
          <tr className="header">
            <th style={{ textAlign: "left" }}>Use case</th>
            <th style={{ textAlign: "left" }}>Application</th>
            <th style={{ textAlign: "left" }}>Comment</th>
          </tr>
        </thead>
        <tbody>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>Web Browser</td>
            <td style={{ textAlign: "left" }}>
              Firefox, Chromium, qutebrowser
            </td>
            <td style={{ textAlign: "left" }}>
              We recommend using Chromium instead of Google Chrome on Linux
            </td>
          </tr>
          <tr className="even">
            <td style={{ textAlign: "left" }}>Mail</td>
            <td style={{ textAlign: "left" }}>
              Thunderbird, KMail (KDE), Evolution (GNOME), mutt (Terminal)
            </td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>Instant Messaging</td>
            <td style={{ textAlign: "left" }}>
              Telegram, Pidgin, Empathy (GNOME)
            </td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="even">
            <td style={{ textAlign: "left" }}>Torrent client</td>
            <td style={{ textAlign: "left" }}>
              Transmission (GNOME), Ktorrent (KDE), Deluge, rtorrent (Terminal)
            </td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>Voice chat</td>
            <td style={{ textAlign: "left" }}>Mumble, Empathy (GNOME)</td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="even">
            <td style={{ textAlign: "left" }}>File synchronization</td>
            <td style={{ textAlign: "left" }}>
              Syncthing, Nextcloud, OwnCloud, Seafile
            </td>
            <td style={{ textAlign: "left" }} />
          </tr>
        </tbody>
      </table>
      <h2 data-number="5.6" id="system">
        <span className="header-section-number">5.6</span> System
      </h2>
      <table>
        <colgroup>
          <col style={{ width: "33%" }} />
          <col style={{ width: "33%" }} />
          <col style={{ width: "33%" }} />
        </colgroup>
        <thead>
          <tr className="header">
            <th style={{ textAlign: "left" }}>Use case</th>
            <th style={{ textAlign: "left" }}>Application</th>
            <th style={{ textAlign: "left" }}>Comment</th>
          </tr>
        </thead>
        <tbody>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>Disk usage</td>
            <td style={{ textAlign: "left" }}>
              Baobab (GNOME), Filelight (KDE), ncdu (Terminal)
            </td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="even">
            <td style={{ textAlign: "left" }}>Virtualization</td>
            <td style={{ textAlign: "left" }}>
              virt-manager, VirtualBox, GNOME Boxes
            </td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>Backup</td>
            <td style={{ textAlign: "left" }}>borg backup, Déjà Dup</td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="even">
            <td style={{ textAlign: "left" }}>System monitor</td>
            <td style={{ textAlign: "left" }}>htop (Terminal)</td>
            <td style={{ textAlign: "left" }}>
              The desktop environment have their own system monitor included.
            </td>
          </tr>
        </tbody>
      </table>
      <h2 data-number="5.7" id="development">
        <span className="header-section-number">5.7</span> Development
      </h2>
      <table>
        <colgroup>
          <col style={{ width: "33%" }} />
          <col style={{ width: "33%" }} />
          <col style={{ width: "33%" }} />
        </colgroup>
        <thead>
          <tr className="header">
            <th style={{ textAlign: "left" }}>Use case</th>
            <th style={{ textAlign: "left" }}>Application</th>
            <th style={{ textAlign: "left" }}>Comment</th>
          </tr>
        </thead>
        <tbody>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>IDE</td>
            <td style={{ textAlign: "left" }}>
              IntelliJ, Eclipse, Code::Blocks, Android Studio, Geany, Qt
              Creator, Gnome Builder
            </td>
            <td style={{ textAlign: "left" }}>
              Some of these might not be Free and Open Source!
            </td>
          </tr>
        </tbody>
      </table>
      <h2 data-number="5.8" id="other">
        <span className="header-section-number">5.8</span> Other
      </h2>
      <table>
        <colgroup>
          <col style={{ width: "33%" }} />
          <col style={{ width: "33%" }} />
          <col style={{ width: "33%" }} />
        </colgroup>
        <thead>
          <tr className="header">
            <th style={{ textAlign: "left" }}>Use case</th>
            <th style={{ textAlign: "left" }}>Application</th>
            <th style={{ textAlign: "left" }}>Comment</th>
          </tr>
        </thead>
        <tbody>
          <tr className="odd">
            <td style={{ textAlign: "left" }}>Dropdown terminal</td>
            <td style={{ textAlign: "left" }}>Guake (GNOME), Yakuake (KDE)</td>
            <td style={{ textAlign: "left" }} />
          </tr>
          <tr className="even">
            <td style={{ textAlign: "left" }}>Clipboard manager</td>
            <td style={{ textAlign: "left" }}>
              Glipper (GNOME), Clipman (XFCE)
            </td>
            <td style={{ textAlign: "left" }}>
              The desktop environment each have their own clipboard manager.
            </td>
          </tr>
        </tbody>
      </table>
      <h1 data-number={6} id="troubleshooting">
        <span className="header-section-number">6</span> Troubleshooting
      </h1>
      <p>
        A chapter for some advanced troubleshooting. If you need something
        contained in this chapter, better ask some advanced user for help (or
        one of our helpers if you are at our Install Event).
      </p>
      <h2 data-number="6.1" id="microsoft-surface">
        <span className="header-section-number">6.1</span> Microsoft Surface
      </h2>
      <h3 data-number="6.1.1" id="something-doesnt-work">
        <span className="header-section-number">6.1.1</span> Something doesn’t
        work
      </h3>
      <p>
        Microsoft Surface devices usually require a patched kernel.{" "}
        <a href="https://github.com/linux-surface/linux-surface">
          See this project
        </a>{" "}
        to install such a kernel.
      </p>
      <h2 data-number="6.2" id="wireless">
        <span className="header-section-number">6.2</span> Wireless
      </h2>
      <h3 data-number="6.2.1" id="broadcom-on-ubuntu-doesnt-work">
        <span className="header-section-number">6.2.1</span> Broadcom on Ubuntu
        doesn’t work
      </h3>
      <p>
        <a href="https://help.ubuntu.com/community/WifiDocs/Driver/bcm43xx">
          See this.
        </a>
      </p>
      <h3 data-number="6.2.2" id="installing-broadcom-firmware">
        <span className="header-section-number">6.2.2</span> Installing Broadcom
        Firmware
      </h3>
      <p>
        <em>openSUSE:</em>
      </p>
      <p>
        <code>sudo zypper install b43-fwcutter</code>{" "}
        <code>sudo /usr/sbin/install_bcm43xx_firmware</code>
      </p>
      <p>
        <a href="https://www.opensuse-guide.org/wlan.php">
          See also this site.
        </a>
      </p>
      <p>
        <em>Ubuntu:</em>
      </p>
      <p>
        <code>sudo apt install firmware-b43-installer</code>
      </p>
      <p>
        <a href="http://linuxwireless.org/en/users/Drivers/b43/">
          See also this site.
        </a>
      </p>
      <h2 data-number="6.3" id="opensuse">
        <span className="header-section-number">6.3</span> OpenSUSE
      </h2>
      <h3
        data-number="6.3.1"
        id="installing-broadcom-wireless-drivers-broadcom-wl"
      >
        <span className="header-section-number">6.3.1</span> Installing Broadcom
        Wireless Drivers (broadcom-wl)
      </h3>
      <ul>
        <li>
          Add the packman repo: YaST -&gt; Software repos -&gt; Add -&gt; Select
          community -&gt; repos -&gt; Next -&gt; Select packman -&gt; Finish
        </li>
        <li>
          <code>zypper ref</code>
        </li>
        <li>
          <code>zypper install broadcom-wl</code>
        </li>
      </ul>
      <h2 data-number="6.4" id="graphics-drivers">
        <span className="header-section-number">6.4</span> Graphics drivers
      </h2>
      <h2 data-number="6.5" id="system-hangs-at-boot-nvidia-present">
        <span className="header-section-number">6.5</span> System hangs at boot
        (NVIDIA present)
      </h2>
      <p>
        Boot with <code>nomodeset</code> kernel parameter, then either install
        proprietary NVIDIA drivers or disable the NVIDIA graphics entirely.
      </p>
      <h2 data-number="6.6" id="i-want-to-disable-the-nouveau-driver">
        <span className="header-section-number">6.6</span> I want to disable the
        Nouveau driver
      </h2>
      <p>
        <code>
          echo 'blacklist nouveau' | tee /etc/modprobe.d/blacklist.conf
        </code>
      </p>
      <h2 data-number="6.7" id="acer">
        <span className="header-section-number">6.7</span> Acer
      </h2>
      <p>
        Some types of newer Acer notebooks need special settings for Secure Boot
        in order to boot an installed GNU/Linux system at all. If you installed
        eg. Ubuntu but it directly boots Windows without giving you a GRUB
        selection screen, try the following guide:
        https://askubuntu.com/questions/771455/dual-boot-ubuntu-with-windows-on-acer-aspire/771749#771749
      </p>
      <h2 data-number="6.8" id="system-1">
        <span className="header-section-number">6.8</span> System
      </h2>
      <h3 data-number="6.8.1" id="chroot-into-an-installed-system">
        <span className="header-section-number">6.8.1</span> chroot into an
        installed system
      </h3>
      <p>
        To chroot into an installed system from a live iso, do the following:
      </p>
      <ul>
        <li>
          <code>sudo su</code>
        </li>
        <li>
          <code>mount /dev/sdaX /mnt</code>, where <code>sdaX</code> is your
          root partition
        </li>
        <li>
          If you have EFI: <code>mount /dev/sda1 /mnt/boot/efi</code> (assuming{" "}
          <code>sda1</code> is your EFI partition)
        </li>
        <li>
          <code>mount -t proc proc /mnt/proc/</code>
        </li>
        <li>
          <code>mount -t sysfs sys /mnt/sys/</code>
        </li>
        <li>
          <code>mount -o bind /dev /mnt/dev/</code>
        </li>
        <li>
          <code>chroot /mnt /bin/bash</code>
        </li>
      </ul>
      <h3 data-number="6.8.2" id="cleaning-up-boot-entries">
        <span className="header-section-number">6.8.2</span> Cleaning up boot
        entries
      </h3>
      <ul>
        <li>
          <code>efibootmgr</code> prints all boot entries.
        </li>
        <li>
          <code>efibootmgr -o XXXX,YYYY,ZZZZ</code> sets the boot order.
        </li>
        <li>
          <code>efibootmgr -b &lt;number&gt; -B</code> deletes an EFI boot
          entry.
        </li>
      </ul>
      <h3 data-number="6.8.3" id="drives-are-not-found-in-the-installer">
        <span className="header-section-number">6.8.3</span> Drives are not
        found in the installer
      </h3>
      <p>
        Especially newer laptops (2018+) have options to switch between RAID and
        AHCI boot mode.
      </p>
      <p>
        If the laptop is running in RAID mode and Linux does not recognize its
        drives, it will have to be switched to AHCI. Unfortunately, this also
        means Windows needs to be reconfigured, otherwise it won’t boot anymore.
      </p>
      <ol type={1}>
        <li>
          Click the Start Button and type <code>cmd</code>
        </li>
        <li>
          Right-click the result and select <em>Run as administrator</em>
        </li>
        <li>
          Type this command and press <em>ENTER:</em>{" "}
          <code>
            bcdedit /set {"{"}current{"}"} safeboot minimal
          </code>{" "}
          (ALT: <code>bcdedit /set safeboot minimal</code>)
        </li>
        <li>Restart the computer and enter BIOS Setup</li>
        <li>Change the SATA Operation mode to AHCI from either IDE or RAID</li>
        <li>
          Save changes and exit Setup and Windows will automatically boot to
          Safe Mode.
        </li>
        <li>
          Right-click the Windows Start Menu once more. Choose Command Prompt
          (Admin).
        </li>
        <li>
          Type this command and press ENTER:{" "}
          <code>
            bcdedit /deletevalue {"{"}current{"}"} safeboot
          </code>{" "}
          (ALT: bcdedit /deletevalue safeboot)
        </li>
        <li>
          Reboot once more and Windows will automatically start with AHCI
          drivers enabled.
        </li>
      </ol>
      <h3 data-number="6.8.4" id="bit-efi-is-used">
        <span className="header-section-number">6.8.4</span> 32-bit EFI is used
      </h3>
      <ul>
        <li>No provided install image will boot.</li>
        <li>Windows is installed in 32-bit mode.</li>
        <li>Some older Macbooks have this configuration.</li>
        <li>
          To install 64-bit Linux (we never encountered an actual 32-bit CPU
          with this problem!), manually replace the bootloader with a grub
          standalone.{" "}
          <a href="https://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface#Booting_64-bit_kernel_on_32-bit_UEFI">
            See the Arch Wiki.
          </a>
        </li>
      </ul>
      <h3 data-number="6.8.5" id="i-need-to-turn-off-bitlocker">
        <span className="header-section-number">6.8.5</span> I need to turn off
        Bitlocker
      </h3>
      <p>
        <strong>
          Do not change any BIOS/UEFI settings before disabling BitLocker!
        </strong>{" "}
        You will have to provide the decryption key otherwise, which the user
        typically has no access to.
      </p>
      <ul>
        <li>
          Launch a command prompt with administrator rights and use{" "}
          <code>manage-bde -off C:</code>, where <code>C:</code> is the drive
          you want to decrypt.
        </li>
        <li>
          Use the command <code>manage-bde -status</code> to query the
          decryption status.
        </li>
        <li>
          You will need to wait until decryption is complete, which can take a
          long time.
        </li>
      </ul>
      <h3 data-number="6.8.6" id="unlock-bitlocker-encrypted-devices">
        <span className="header-section-number">6.8.6</span> Unlock BitLocker
        Encrypted Devices
      </h3>
      <p>
        If you change any UEFI settings on a BitLocker encrypted device
        (typically Surface devices), you will be prompted for the BitLocker key
        on next boot.
      </p>
      <p>
        Since Surface devices come encrypted out of the box, the user does
        typically not have that key and Windows will refuse to boot. If this
        happens, resetting the UEFI settings to factory settings should fix the
        issue.
      </p>
      <p>
        Alternatively, you can just enter the correct Bitlocker key. This works
        only if the user has a Microsoft account linked to the device. You can
        get the BitLocker key as follows:
      </p>
      <ul>
        <li>
          On another device, google for “BitLocker Recovery Key”. You should
          find several Microsoft Support or FAQ pages on how to recover the key.
        </li>
        <li>
          Search for a link saying “To get your recovery key, go to BitLocker
          Recovery Keys” or similar. Go there.
        </li>
        <li>
          Ask the user to sign in using their Microsoft account. The website
          will then display their recovery key, which can be used to unlock the
          device.
        </li>
      </ul>
      <h3 data-number="6.8.7" id="system-will-not-boot-under-any-circumstance">
        <span className="header-section-number">6.8.7</span> System will not
        boot under any circumstance
      </h3>
      <p>
        Some very bad firmwares just refuse to boot GRUB, however you configure
        it. This “bootloader hack” can be applied in these cases.
      </p>
      <ul>
        <li>
          Try to boot the actual distro that has been installed by “using a
          device” to boot. Do so by holding shift while clicking on “reboot” in
          Windows.
        </li>
        <li>
          If that doesn’t work, boot a live system and chroot into the installed
          system.
        </li>
        <li>
          Once booted or chrooted into the Linux system, become root (sudo su)
          and go to <code>/boot/efi/EFI/Microsoft/Boot</code> and locate a file
          named <code>bootmgfw.efi</code>. Rename it to{" "}
          <code>bootmgfw-original.efi</code>.
        </li>
        <li>
          Go to <code>/boot/efi/EFI/grub2</code> (sometimes also just{" "}
          <code>grub</code>) and locate the file <code>grubx64.efi</code>. Copy
          it over to <code>/boot/efi/EFI/Microsoft/Boot/</code>. If a file
          called <code>shimx64.efi</code> exists, copy that one over as well.
        </li>
        <li>
          Find the file <code>grub.cfg</code> in{" "}
          <code>/boot/efi/EFI/grub2</code> and copy it over to{" "}
          <code>/boot/efi/EFI/Microsoft/Boot/</code>.
        </li>
        <li>
          Go back to <code>/boot/efi/EFI/Microsoft/Boot/</code>. If{" "}
          <code>shimx64.efi</code> exists, rename it to{" "}
          <code>bootmgfw.efi</code>. If it does not exist, rename{" "}
          <code>grubx64.efi</code> to <code>bootmgfw.efi</code>.
        </li>
        <li>
          Now go to <code>/boot/grub/</code>, or <code>/boot/opensuse/</code>{" "}
          (the exact folder path may vary). Find the file <code>grub.cfg</code>{" "}
          and open it. Find the <code>menuentry</code> block for Windows
          (usually called “Windows Bootloader (on /dev/sdx)” or similar). Copy
          the entire block.
        </li>
        <li>
          Uninstall the package <code>os-prober</code>.
        </li>
        <li>
          Now go to /etc/grub.d. If a file with <code>os-prober</code> in its
          name exists, delete it.
        </li>
        <li>
          Find the file <code>40-custom.cfg</code>. If it doesn’t exist, create
          it. Paste the menuentry block you copied earlier in this file.
        </li>
        <li>
          In the text you just pasted, look for <code>bootmgfw.efi</code> and
          change it to <code>bootmgfw-original.efi</code>.
        </li>
        <li>Save the file.</li>
        <li>
          Run <code>grub-mkconfig -o /boot/grub/grub.cfg</code>. Make sure the
          file path matches the path where you originally found the{" "}
          <code>grub.cfg</code> you copied the menuentry from.
        </li>
        <li>
          Reboot and verify that grub now loads properly. Also test whether
          Windows boots!
        </li>
      </ul>
    </>
  );
}
