import { useSession, signIn, signOut } from "next-auth/react";

import { Button, useMantineTheme } from "@mantine/core";

import { Icon, ICONS } from "vseth-canine-ui";

import { getAccentColor } from "../utilities/colors";

const LoginButton = () => {
  const { data: session } = useSession();
  const theme = useMantineTheme();

  if (session) {
    return (
      <Button
        leftIcon={<Icon icon={ICONS.LEAVE} color={getAccentColor(theme)} />}
        variant="light"
        onClick={() => signOut()}
      >
        Logout
      </Button>
    );
  }
  return (
    <Button
      leftIcon={<Icon icon={ICONS.ENTER} color={getAccentColor(theme)} />}
      variant="light"
      onClick={() => signIn("keycloak", { callbackUrl: "/" })}
    >
      Login
    </Button>
  );
};

export default LoginButton;
