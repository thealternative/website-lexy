import { useSearchParams } from "next/navigation";

import { useEffect } from "react";

import { useSession, signIn } from "next-auth/react";

import { useI18n, useCurrentLocale } from "/locales/client";

import {
  Alert,
  Button,
  Card,
  Group,
  List,
  Space,
  useMantineTheme,
} from "@mantine/core";
import { notifications } from "@mantine/notifications";
import { useScrollIntoView } from "@mantine/hooks";

import {
  IconList,
  IconUser,
  IconMapPin,
  IconLink,
  IconUserPlus,
  IconUserMinus,
  IconExternalLink,
  IconCalendar,
  IconUsersGroup,
  IconEdit,
  IconClock,
  IconCheck,
} from "@tabler/icons-react";

import parse from "html-react-parser";

import hasAccess from "/utilities/hasAccess";
import { getAccentColor } from "/utilities/colors";
import { formatDateFromDB, formatTimeFromDB } from "/utilities/dates";
import { isRegistered } from "/utilities/signUp";

import { gql, useMutation } from "@apollo/client";

const addSignUpMutation = gql`
  mutation addSignUp($id: Int) {
    addSignUp(id: $id)
  }
`;

const removeSignUpMutation = gql`
  mutation removeSignUp($id: Int) {
    removeSignUp(id: $id)
  }
`;

export default function EventCard({
  event,
  setEvent,
  setOpen,
  refetch,
  setSignUpOpen,
  user,
}) {
  const [addSignUp] = useMutation(addSignUpMutation);
  const [removeSignUp] = useMutation(removeSignUpMutation);

  const { data: session } = useSession();
  const t = useI18n();
  const locale = useCurrentLocale();
  const theme = useMantineTheme();

  const searchParams = useSearchParams();
  const eventId = searchParams.get("eventId");
  const { scrollIntoView, targetRef } = useScrollIntoView({ offset: 20 });
  const highlight = eventId
    ? eventId === String(event.id)
      ? true
      : false
    : false;

  useEffect(() => {
    if (highlight) {
      scrollIntoView();
    }
  }, [highlight]);

  const accentColor = getAccentColor(theme);
  const signedUp = isRegistered(user, event);

  const signUp = async (id) => {
    if (!session) signIn("keycloak", { callbackUrl: "/" });
    const status = await addSignUp({
      variables: {
        id: event.id,
      },
    });
    refetch();
    if (status.data.addSignUp) {
      notifications.show({
        title: t("successfulSignup"),
        message: t("successfulSignupText"),
        color: "green",
      });
    } else {
      notifications.show({
        title: t("alreadySignedUp"),
        message: t("alreadySignedUpText"),
        color: "green",
      });
    }
  };

  const signOff = async (id) => {
    await removeSignUp({
      variables: {
        id: event.id,
      },
    });
    refetch();
    notifications.show({
      title: t("successfulSignoff"),
      message: t("successfulSignoffText"),
      color: "green",
    });
  };

  const editEvent = (event) => {
    setEvent(event);
    setOpen(true);
  };

  const viewSignUps = (event) => {
    setEvent(event);
    setSignUpOpen(true);
  };

  const copyLink = (event) => {
    const baseUrl =
      process.env.NEXT_PUBLIC_DEPLOYMENT === "dev"
        ? "https://www.thealt.staging-sip.ethz.ch"
        : "https://thealternative.ch";
    navigator.clipboard.writeText(baseUrl + "/?eventId=" + String(event));
    notifications.show({
      title: t("copied"),
      message: t("copiedText"),
      color: "green",
    });
  };

  return (
    <Card
      shadow="md"
      radius="md"
      style={{
        display: "flex",
        justifyContent: "space-between",
        flexDirection: "column",
        width: "100%",
        color: event.isStammtisch ? "" : "",
        boxShadow: highlight
          ? "4px 4px 16px 0 #f28a20, -4px -4px 16px 0 #f28a20, -4px 4px 16px 0 #f28a20, 4px -4px 16px 0 #f28a20"
          : "none",
      }}
      ref={highlight ? targetRef : null}
    >
      <div>
        <div className="flex gap-4">
          <h2 className="font-bold text-3xl">{event.title}</h2>
          <button
            onClick={() => copyLink(event.id)}
            style={{ color: "#f28a20" }}
          >
            <IconLink color="#f28a20" />
          </button>
        </div>

        {signedUp && (
          <>
            <Space h="xs" />
            <Alert
              color="green"
              variant="light"
              icon={<IconCheck />}
              title={t("signedUpTitle")}
            >
              {t("signedUpText")}
            </Alert>
          </>
        )}

        <Space h="lg" />
        <List
          spacing="md"
          size="md"
          center
          style={{ color: event.isStammtisch ? "" : "" }}
        >
          {event.speaker && (
            <List.Item icon={<IconUser color="#f28a20" />}>
              <p>{event.speaker}</p>
            </List.Item>
          )}
          <List.Item icon={<IconMapPin color="#f28a20" />}>
            <p>{event.place}</p>
          </List.Item>
          <List.Item icon={<IconCalendar color="#f28a20" />}>
            <p>{formatDateFromDB(event.date, locale)}</p>
          </List.Item>
          <List.Item icon={<IconClock color="#f28a20" />}>
            <p>{formatTimeFromDB(event.startTime, event.endTime)}</p>
          </List.Item>
          {hasAccess(session, true) && !event.isStammtisch && (
            <List.Item icon={<IconUsersGroup color="#f28a20" />}>
              <p>
                {event.signUps.length} {t("signUps")}
              </p>
            </List.Item>
          )}
        </List>
        <Space h="lg" />

        <p>{parse(event.description)}</p>
      </div>
      <div>
        {event.isStammtisch ? (
          <Button
            disabled
            fullWidth
            leftSection={<IconUserPlus color="gray" />}
          >
            {t("noSignupNeeded")}
          </Button>
        ) : (
          <Group mt="xl" style={{ verticalAlign: "middle" }} grow>
            {event.signUp ? (
              <Button
                href={event.signUp}
                target="_blank"
                rightIcon={<IconExternalLink color="#f28a20" />}
                component="a"
              >
                {t("externalSignUp")}
              </Button>
            ) : (
              <>
                {signedUp ? (
                  <Button
                    onClick={() => signOff(event.id)}
                    leftSection={<IconUserMinus color="white" />}
                  >
                    {t("signOff")}
                  </Button>
                ) : (
                  <Button
                    onClick={() => signUp(event.id)}
                    leftSection={<IconUserPlus color="#f28a20" />}
                    style={{ color: "#f28a20" }}
                  >
                    {t("signUp")}
                  </Button>
                )}
              </>
            )}
          </Group>
        )}
        {hasAccess(session, true) && (
          <Group grow mt="md" style={{ verticalAlign: "middle" }}>
            <Button
              leftSection={<IconEdit />}
              variant="light"
              onClick={() => editEvent(event)}
            >
              {t("editEvent")}
            </Button>
            {!event.isStammtisch && (
              <Button
                leftSection={<IconList />}
                variant="light"
                onClick={() => viewSignUps(event)}
              >
                {t("viewParticipants")}
              </Button>
            )}
          </Group>
        )}
      </div>
    </Card>
  );
}
