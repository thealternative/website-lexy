export default function sitemap() {
  return [
    {
      url: 'https://thealternative.ch',
      lastModified: new Date(),
      changeFrequency: 'yearly',
      priority: 1,
      alternates: {
        languages: {
          de: 'https://thealternative.ch/de',
          en: 'https://thealternative.ch/en',
        }
      }
    }
  ]
}
