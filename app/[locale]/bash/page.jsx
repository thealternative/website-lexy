"use client";

import "/app/guides.css";

import dynamic from "next/dynamic";
import { useState, useEffect } from "react";

import { Alert, Container } from "@mantine/core";

import { IconInfoCircle } from "@tabler/icons-react";

import BashGuide from "/components/bashguide";
const TOC = dynamic(() => import("/components/toc"), {
  ssr: false,
});

export default function Bash({ params: { locale } }) {
  return (
    <>
      <Container size="xl" className="my-8">
        <div className="grid gap-4 grid-cols-1 md:grid-cols-3">
          <div className="md:block hidden">
            <TOC />
          </div>
          <div className="bash-guide col-span-1 md:col-span-2">
            {locale === "de" && (
              <Alert icon={<IconInfoCircle />} title="Bemerkung">
                Leider ist dieser Guide nur auf Englisch verfügbar.
              </Alert>
            )}
            <BashGuide />
          </div>
        </div>
      </Container>
    </>
  );
}
