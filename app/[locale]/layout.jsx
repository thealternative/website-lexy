import "/app/globals.css";
import "@mantine/core/styles.css";
import "@mantine/dates/styles.css";
import "@mantine/notifications/styles.css";
import "@mantine/tiptap/styles.css";

import { Source_Sans_3 } from "next/font/google";

import { ColorSchemeScript, createTheme } from "@mantine/core";

import { getStaticParams } from "/locales/server";
import { setStaticParamsLocale } from "next-international/server";

import I18nWrapper from "/components/i18nWrapper";
import Navbar from "/components/navbar";
import Footer from "/components/footer";

const ss3 = Source_Sans_3({
  subsets: ["latin"],
  display: "swap",
});

export async function generateMetadata(
  { params, searchParams },
  parent
) {
  // read route params
  const locale = params.locale;

  const description = locale === "de" ?
    "Wir sind Studierende der ETH, die dir an unseren Events und in unserem Büro kostenlos mit Linux helfen, weil wir Open Source und Freie Software lieben und Dir den Einstieg erleichtern wollen. Jedes Semester organisieren wir Events, die Dich bei der Nutzung und Produktion von freier und offener Wissenschaft, Hardware, Software und Dateiformaten unterstützen." :
    "We are students at ETH who help you for free with Linux at our events and in our office, because we love Open Source and Free Software and want to help you to get started. Each semester, we organize events to help you use and produce Free and Open Science, Hardware, Software and File Formats.";
  return {
    title: "TheAlternative",
    description,
    openGraph: {
      images: ["/opengraph-image.png"]
    },
    metadataBase: new URL("https://thealternative.ch/"),
  }
}

export function generateStaticParams() {
  return getStaticParams();
}
export default async function RootLayout({ children, params }) {
  const locale = params?.locale || "en";

  setStaticParamsLocale(locale || "en");

  const theme = createTheme({
    colors: {
      blue: [
        "#d6e4f7",
        "#b8c9e0",
        "#9aaeca",
        "#7d94b4",
        "#5f799d",
        "#415e87",
        "#244471",
        "#193358",
        "#0e223f",
        "#041126",
      ],
      orange: [
        "#f2c28a",
        "#f2b878",
        "#f2af66",
        "#f2a655",
        "#f29c43",
        "#f29331",
        "#f28a20",
        "#c26f19",
        "#935413",
        "#64390d",
      ],
    },
    primaryColor: "blue",
    primaryShade: {
      light: 6,
      dark: 5,
    },
  });

  return (
    <html lang={locale} className={ss3.className}>
      <head>
        <ColorSchemeScript defaultColorScheme="auto" />
      </head>
      <body>
        <div
          className={`flex min-h-screen flex-col justify-between bg-white dark:bg-zinc-900 dark:text-white relative ${ss3.className}`}
        >
          <Navbar locale={locale} />
          <main className="grow">
            <I18nWrapper locale={locale} theme={theme}>
              {children}
            </I18nWrapper>
          </main>
          <Footer />
        </div>
      </body>
    </html>
  );
}
