import nodemailer from "nodemailer";

import config from "/thealternative.config";

export async function POST(req, res) {
  const { name, mail, message, human, robot } = req.body;

  if (
    human == true &&
    robot == false &&
    name != "" &&
    mail != "" &&
    message != ""
  ) {
    const transporter = nodemailer.createTransport({
      host: process.env.MAILER_HOST,
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: process.env.MAILER_USERNAME,
        pass: process.env.MAILER_PASSWORD,
      },
    });

    await transporter.sendMail({
      from: process.env.MAILER_NAME,
      to: config.address,
      replyTo: mail,
      subject: (config.subject || "").replace("NAME", name),
      html: message,
    });

    res.status(200).json({ message: "mailSuccess" });
  } else {
    res.status(200).json({ message: "mailFailure" });
  }
}
