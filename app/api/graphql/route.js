import { createSchema, createYoga } from "graphql-yoga";
import { typeDefs } from "/graphql/schema";
import { resolvers } from "/graphql/resolvers";
import { createContext } from "/graphql/context";

import { getServerSession } from "next-auth/next";
import { getToken } from "next-auth/jwt";
import authOptions from "/lib/authOptions";

const { handleRequest } = createYoga({
  schema: createSchema({
    typeDefs,
    resolvers,
    //context: createContext,
  }),
  context: async (context) => {
    const token = await getToken({ req: context.request });

    return {
      session: token,
    };
  },
  graphqlEndpoint: "/api/graphql",
});

export { handleRequest as GET, handleRequest as POST };
