import KeycloakProvider from "next-auth/providers/keycloak";
const jwt = require("jsonwebtoken");

import prisma from "/lib/prisma";

const authOptions = {
  providers: [
    KeycloakProvider({
      clientId: process.env.SIP_AUTH_WEBSITE_CLIENT_ID,
      clientSecret: process.env.SIP_AUTH_WEBSITE_CLIENT_SECRET,
      issuer: process.env.SIP_AUTH_OIDC_ISSUER,
    }),
  ],
  callbacks: {
    async jwt({ token, account, profile }) {
      if (account) {
        const tok = account.access_token;
        const decoded = jwt.decode(tok, { complete: true });
        return {
          ...token,
          info: decoded,
        };
      }
      return token;
    },
    async session({ session, token, user }) {
      const u = await prisma.user.findUnique({
        where: {
          email: token.email,
        },
      });

      if (!u) {
        await prisma.user.create({
          data: {
            name: token.name,
            email: token.email,
          },
        });
      }

      const sesh = {
        ...session,
        info: token.info,
      };
      return sesh;
    },
  },
};

export default authOptions;
