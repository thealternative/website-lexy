export function isRegistered(user, event) {
  if (!user) return false;
  const matches = event.signUps.filter((signUp) => signUp.user.id === user.id);
  if (matches.length > 0) return true;
  return false;
}
