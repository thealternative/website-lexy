export function getAccentColor(theme) {
  return theme.colorScheme === "dark" ? "white" : "#244471";
}
