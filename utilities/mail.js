import nodemailer from "nodemailer";

export async function sendMail(receiver, replyTo, html, subject) {
  const transporter = nodemailer.createTransport({
    host: process.env.MAILER_HOST,
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: process.env.MAILER_USERNAME,
      pass: process.env.MAILER_PASSWORD,
    },
  });

  await transporter.sendMail({
    from: process.env.MAILER_NAME,
    to: receiver,
    replyTo: replyTo,
    subject: subject,
    html: html,
  });

  return;
}
